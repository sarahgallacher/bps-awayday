#BPS Away Day

Step-by-step instructions for the BPS away day activities

---

##Activity 1: End node - Rapsberry Pi
* Take Raspberry Pi (Rpi) out of the box and look at its different connections - identified in the picture below

![Scheme](images/pi_ports.jpg)

* Plug the HDMI cable into the Rpi HDMI port (see picture below) and the other end into the screen
* Plug the keyboard into a USB hub (white thing with USB ports) and then plug the USB hub into the Rpi micro USB port (see picture below). Make sure to use the correct USB port - not the power supply one!
* Now plug the power supply into a plug socket and plug the micro USB end into the Rpi power port (see picture below). Make sure to use the right port! 

![Scheme](images/pi_connections.jpg)

* The screen should come to life showing the Rpi boot messages and finish with a prompt for username and password.
* Enter the username: `pi` and the password `raspberry` (note that nothing will show on the screen as you type the password)

The Rpi is a mini Linux computer with a full Operating System (OS). You are currently in the home directory and you can see all sub-directories (including Documents, Desktop, etc.) by typing the command: `ls`  
As well as being a computer, the Rpi also has a number of GPIO pins (see picture) to which you can attach electronic components and then control them via code in the Rpi. In the next few examples we will explore different types of input and output components that we can attach to the GPIO pins and control them with Python code.  

###Blink LED
This example demonstrates digital output - where an component is in 1 of 2 states. In this case the LED can be OFF or ON.

* Navigate to the folder containing example code for this workshop by typing: `cd ~/dev/bps-awayday/examples`
* Open the Nano text editor to view the code for the blink example by typing: `nano blink.py` (you can also view the code in your browser by clicking on source (left menu) -> examples -> blink.py)
* Explore the code to understand what it is doing - can you identify the part that dictates the duration of each LED blink?
* To close the Nano text editor type: `CTRL + x`
* Now take the Rpi, breadboard and the following components shown in the picture below:
    * 2 x wires
    * 1 x led
    * 1 x 220ohm resistor
    
![Scheme](images/blink_components.jpg)

* Hook up the Rpi, breadboard and components as shown in the images below
    * Make sure that the LED is the right way round. The long leg is positive and should connect to the resistor. The short leg is ground and should connect to the wire (black) going back to the pi zero ground pin.
    * Connect the ground wire (black) to the ground pin on the Rpi (bottom row, 3 from the right), and connect the pin wire (brown) to pin number 17 on the Rpi (top row, 6 from the right).

![Scheme](images/blink_wiring.jpg)

![Scheme](images/pi_pinout_blink.png)

* The LED is connected to pin 17 which relates to `ledPin = LED(17)` in the code.
* Once you have connected everything up as shown in the image, type the following command to run the code: `python blink.py` (there will be a little delay while the code initialises - you will see `Ready` on the screen when the code has initialised and is starting to run)
* You should see the LED blink off and on at 1 second intervals and the text `Turning LED on` and `Turning LED off` appear on the screen as the LED goes on and off.
* To stop the code running type: `CTRL + c`
* Try editing the code to change the blink interval and re-run it again.
    * Open Nano text editor: `nano blink.py` and make changes to your code
    * To save your changes type: `CTRL + o`, then hit `return` to confirm saving
    * To close Nano type `CTRL + x`
    * To run code type `python blink.py`

###Button
This example demonstrates digital input - where input received from a component is one of 2 values. In this case the button can be PRESSED or RELEASED.  

* Navigate to the folder containing example code for this workshop by typing: `cd ~/dev/bps-awayday/examples`
* Open the Nano text editor to view the code for the blink example by typing: `nano button.py` (you can also view the code in your browser by clicking on source (left menu) -> examples -> button.py)
* Explore the code to understand what it is doing. This code includes 2 *methods*. One gets called when the button is pressed and the other gets called when the button is released.
* To close the Nano text editor type: `CTRL + x`
* Now take the Rpi, breadboard and the following components shown in the picture below:
    * 1 x wire
    * 1 x button

![Scheme](images/button_components.jpg)

* Hook up the Rpi, breadboard and components as shown in the images below
    * It doesn't matter what way round the button goes, just make sure that one of the legs lines up with the ground wire (black) that goes to the ground pin on the Rpi.
    * Connect one end of the pin wire (yellow) to the other leg of the button. Connect the pin wire to pin 2 on the Rpi (top row, 2 from the right).

![Scheme](images/button_wiring.jpg)

![Scheme](images/pi_pinout_button.png)

* The button is connected to pin 2 which relates to `button = Button(2)` in the code.
* Once you have connected everything up as shown in the image, type the following command to run the code: `python button.py` (there will be a little delay while the code initialises - you will see `Ready` on the screen when the code has initialised and is starting to run)
* Push the button and you should see the text `Button pressed` on the screen when you press the button and `Button released` on the screen when you stop pressing.
* To stop the code running type: `CTRL + c`
* Try editing the code to change what text appears when the button is pressed and released, and re-run it again.
    * Open Nano text editor: `nano button.py` and make changes to your code
    * To save your changes type: `CTRL + o`, then hit `return` to confirm saving
    * To close Nano type `CTRL + x`
    * To run code type `python button.py`
* Now try editing the code to make the LED turn ON when you press the button, and turn OFF when you release the button.
    * Open Nano text editor: `nano button.py` and make changes to your code
    * To save your changes type: `CTRL + o`, then hit `return` to confirm saving
    * To close Nano type `CTRL + x`
    * To run code type `python button.py`
* Once you've had a go at changing the button code to control the LED, take a look in `button-led.py` to see a solution:
    * `nano button-led.py`
    * `CTRL + x` to close

###Light sensor
This example demonstrates analog input, where a component provides a continuous value in a range (e.g. 0 to 1, or 0 to 255). In this case the light sensor report higher values when exposed to more light and lower values when exposed to less.  

* Navigate to the folder containing example code for this workshop by typing: `cd ~/dev/bps-awayday/examples`
* Now open the Nano text editor to view code for the light sensor example by typing: `nano light.py` (you can also view the code in your browser by clicking on source -> examples -> light.py)
* Explore the code to understand what is happening - can you identify the part that prints the light sensor value?
* To close the Nano text editor type: `CTRL + x`
* Now take the Rpi, breadboard and the following components shown in the picture below:
    * 1 x light sensor
    * 1 x capacitor
    * 2 x wires

![Scheme](images/light_components.jpg)

* Hook up the Rpi, breadboard and components as shown in the images below
    * Make sure you connect the capacitor the right way round. The long leg is positive and should connect to the light sensor. The other leg is ground and should connect to the wire (black) going back to the pi zero ground pin.
    * It doesn't matter what way round the light sensor goes.
    * Connect the 3V wire (green) to the 3V pin on the Rpi (top row, first pin on the right), and connect the pin wire (blue) to pin number 18 on the Rpi (bottom row, 6 from the right).

![Scheme](images/light_wiring.jpg)

![Scheme](images/pi_pinout_light.png)

* Once you have connected everything up as shown in the image type the following command to run the code: `python light.py`
* There will be a little delay while the code initialises - you will see `Ready` on the screen when the code has initialised and is starting to run
* You should see light sensor values printed on the screen every 1 second
* Try covering and uncovering the sensor to see how the printed value changes
* To stop the code running type: `CTRL + c`
* It is also possible to control the brightness of an LED based on the light sensor values
* Run the example `python light-led.py`
* As you cover and uncover the light sensor, the brightness of the LED will change
    * `CTRL + c` to stop code running

       
###Explore!
* Use the code examples and components to put together a python script that does something interesting with a button, LED and light sensor
* Extra components are available if you want to go bigger!

Lots more code examples and wiring diagrams can be found here:
https://gpiozero.readthedocs.io/en/stable/recipes.html

---

##Activity 2: Communication - Rotronics, Rpi and LoRa

---

##Activity 3: Data Visualisation - Cloud and Grafana