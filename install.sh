#!/bin/sh

if [ "$1" = "python" ]
then
  echo "Installing python requirements..."
  cd python
  sudo pip install -r requirements.txt
  echo "Installing sqlite3 db service"
  mkdir /home/pi/data
  sudo apt-get install sqlite3
  sudo apt-get install libsqlite3-dev
  sudo apt-get install python-gpiozero
elif [ "$1" = "service" ]
then
  echo "Installing IoT service..."
  sudo cp rotronics.cron /etc/cron.d/rotronics
  sudo service cron restart
  sudo cp rotronics.service /lib/systemd/system/
  sudo systemctl enable rotronics.service
  sudo systemctl daemon-reload
  sudo systemctl start rotronics.service
else
  echo "not enough arguments: sh install.sh <python/service>"
fi
