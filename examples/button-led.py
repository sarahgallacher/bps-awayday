from gpiozero import LED
from gpiozero import Button
from signal import pause


led = LED(17) # set LED pin
button = Button(2) # set button pin


def pressed():
    led.on()
    print("Button pressed")


def released():
    led.off()
    print("Button released")


print("Ready\n\n")

button.when_pressed = pressed
button.when_released = released

pause()
