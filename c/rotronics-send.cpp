/*******************************************************************************
 * Copyright (c) 2015 Thomas Telkamp and Matthijs Kooijman
 *
 * Permission is hereby granted, free of charge, to anyone
 * obtaining a copy of this document and accompanying files,
 * to do whatever they want with them without any restriction,
 * including, but not limited to, copying, modification and redistribution.
 * NO WARRANTY OF ANY KIND IS PROVIDED.
 *
 * This example sends a valid LoRaWAN packet with payload "Hello, world!", that
 * will be processed by The Things Network server.
 *
 * Note: LoRaWAN per sub-band duty-cycle limitation is enforced (1% in g1, 
*  0.1% in g2). 
 *
 * Change DEVADDR to a unique address! 
 * See http://thethingsnetwork.org/wiki/AddressSpace
 *
 * Do not forget to define the radio type correctly in config.h, default is:
 *   #define CFG_sx1272_radio 1
 * for SX1272 and RFM92, but change to:
 *   #define CFG_sx1276_radio 1
 * for SX1276 and RFM95.
 *
 *******************************************************************************/

#include <stdio.h>
#include <time.h>
#include <wiringPi.h>
#include <lmic.h>
#include <hal.h>
#include <local_hal.h>
#include <stdlib.h>
#include <sqlite3.h>

// LoRaWAN Application identifier (AppEUI)
// Not used in this example
static const u1_t APPEUI[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

// LoRaWAN DevEUI, unique device ID (LSBF)
// Not used in this example
static const u1_t DEVEUI[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

// network session key 
static const u1_t DEVKEY[16] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

// application session key
static const u1_t ARTKEY[16] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

// device address (DevAddr)
static const u4_t DEVADDR = 0x00000000;

//////////////////////////////////////////////////
// APPLICATION CALLBACKS
//////////////////////////////////////////////////

// provide application router ID (8 bytes, LSBF)
void os_getArtEui (u1_t* buf) {
    memcpy(buf, APPEUI, 8);
}

// provide device ID (8 bytes, LSBF)
void os_getDevEui (u1_t* buf) {
    memcpy(buf, DEVEUI, 8);
}

// provide device key (16 bytes)
void os_getDevKey (u1_t* buf) {
    memcpy(buf, DEVKEY, 16);
}

int sample_rate = 60; //seconds - 2 mins
//int blink_rate = 1; //seconds


//static osjob_t sendjob;
static osjob_t readjob;

// Pin mapping
lmic_pinmap pins = {
  .nss = 11,
  .rxtx = UNUSED_PIN, // Not connected on RFM92/RFM95
  .rst = 7,  // Needed on RFM92/RFM95
  .dio = {27,0,2}
};

int ledPin = 22;

void onEvent (ev_t ev) {
    //debug_event(ev);
    //fprintf(stdout, "Event = %d\n", ev);

    switch(ev) {
      // scheduled data sent (optionally data received)
      // note: this includes the receive window!
      case EV_TXCOMPLETE:
          // use this event to keep track of actual transmissions
          fprintf(stdout, "Event EV_TXCOMPLETE, time: %d\n", millis() / 1000);
          if(LMIC.dataLen) { // data received in rx slot after tx
              //debug_buf(LMIC.frame+LMIC.dataBeg, LMIC.dataLen);
              fprintf(stdout, "Data Received!\n");
          }
          
          //turn off data LED
          //digitalWrite(ledPin, LOW);

          break;
       default:
          break;
    }
}

static void do_send(/*osjob_t* j*/int temp, int humidity, int co2){

    time_t t=time(NULL);
    fprintf(stdout, "[%x] (%ld) %s", hal_ticks(), t, ctime(&t));
    // Show TX channel (channel numbers are local to LMIC)
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & (1 << 7)) {
      fprintf(stdout, "OP_TXRXPEND, not sending");
    } else {
      // Prepare upstream data transmission at the next possible time. 
      LMIC.frame[0] = 0x01;
      LMIC.frame[1] = temp >> 8;
      LMIC.frame[2] = temp;
      LMIC.frame[3] = 0x02;
      LMIC.frame[4] = humidity;
      LMIC.frame[5] = 0x06;
      LMIC.frame[6] = co2 >> 8;
      LMIC.frame[7] = co2;

      LMIC_setTxData2(1, LMIC.frame, 8, 0);
    }

    // Schedule a timed job to run at the given timestamp (absolute system time)
    //os_setTimedCallback(j, os_getTime()+sec2osticks(sample_rate), do_send);
    //os_setCallback(j, do_send); 

    //Next TX is scheduled after TX_COMPLETE event
}


static int callback(void *data, int argc, char **argv, char **azColName){
  int i;
  //fprintf(stdout, "%s:\n", (const char*)data);
  
  //for(i=0; i<argc; i++){
    //printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
  //}
  //printf("\n");

  //fprintf(stdout, "inside callback");

  if(argc > 0){
    do_send(atoi(argv[2]), atoi(argv[3]), atoi(argv[4]));
  }

  return(0);
}


static void do_read(osjob_t* j) {
  //turn on data LED
  digitalWrite(ledPin, HIGH);

  //Define SQL statement
  sqlite3 *db;
  const char* sql = "SELECT ROWID, * FROM readings ORDER BY ROWID DESC LIMIT 1";
  const char* data = "Callback function called";
  char *zErrMsg = 0;
  int rc;

  //database
  rc = sqlite3_open("/home/pi/data/rotronicsDB", &db);

  if(rc) {
    fprintf(stdout, "Can't open database: %s\n", sqlite3_errmsg(db));
  }else{
    fprintf(stdout, "Opened database successfully\n");
    //fprintf(stdout, "executing DB statement\n");
    
    //Execute SQL statement
    rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
    //fprintf(stdout, "DB statement executed\n");

    if ( rc != SQLITE_OK ) {
      //fprintf(stdout, "SQL error\n");
      fprintf(stdout, "SQL error: %s\n", zErrMsg);
      sqlite3_free(zErrMsg);
    } else {
      fprintf(stdout, "Database operation done successfully\n");
    }
    //fprintf(stdout, "closing DB\n");
    sqlite3_close(db);
  }

  //fprintf(stdout, "setting next do_read\n");

  // Schedule a timed job to run at the given timestamp (absolute system time)
  os_setTimedCallback(j, os_getTime()+sec2osticks(sample_rate), do_read);


  //fprintf(stdout, "setting LED OFF\n");

  //turn off indicator LED
  digitalWrite(ledPin, LOW);
}


void setup() {
  // LMIC init
  wiringPiSetup();

  //LED blink
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);

  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();
  // Set static session parameters. Instead of dynamically establishing a session 
  // by joining the network, precomputed session parameters are be provided.
  LMIC_setSession (0x1, DEVADDR, (u1_t*)DEVKEY, (u1_t*)ARTKEY);
  // Disable data rate adaptation
  LMIC_setAdrMode(0);
  // Disable link check validation
  LMIC_setLinkCheckMode(0);
  // Disable beacon tracking
  LMIC_disableTracking ();
  // Stop listening for downstream data (periodical reception)
  LMIC_stopPingable();
  // Set data rate and transmit power (note: txpow seems to be ignored by the library)
  LMIC_setDrTxpow(DR_SF7,14);
  //

  //temp = temp_arg;
  //humidity = humidity_arg;
  //co2 = co2_arg;
}

void loop() {

do_read(&readjob);
//do_send(&sendjob);

  while(1) {
    os_runloop();
//  os_runloop_once();
  }
}


//int main(int argc, char** argv) {
int main(){

  setup();

  //initial delay before first sample
  delay(sample_rate * 1000);

  while (1) {
    loop();
  }
  return 0;
}

