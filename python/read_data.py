

import serial
import time
import sqlite3

# sql_db = '\Users\Sarah.Gallacher\data\rotronicsDB'
sql_db = '/home/pi/data/rotronicsDB'


# **********************************
# Send commands to rotronics device
# **********************************
def send_q():
    command = '51af'
    print("sending command: " + command)
    ser.write(bytearray.fromhex(command))


def send_d():
    command = '649c'
    print("sending command: " + command)
    ser.write(bytearray.fromhex(command))


def send_g():
    command = '53001E43024A'
    print("sending command: " + command)
    ser.write(bytearray.fromhex(command))


# **********************************
# Parse responses from rotronics device
# **********************************
def get_response(delay):
    res = ser.read()  # wait for response to arrive until timeout
    while True:
        time.sleep(delay)
        if ser.in_waiting > 0:
            res += ser.read(ser.in_waiting)
        else:
            return res


def parse_quit(q):
    return q.decode('latin-1')[:1]


def parse_data(data):
    hex_data = data.encode('hex')

    latest_record = []
    index = 0  # handle start entry
    n = 0

    # data arrives in chunks of 20 with start byte and checksum byte
    # at beginning and end of each 20 records
    while index < len(hex_data):

        if n == 0:  # start of next 20 records
            index = index + 2

        timestamp = int(hex_data[index:index+8], 16) + 946684800  # converting 2000 time to UNIX time
        temp = round(((int(hex_data[index+8:index+12], 16) / 10) - 32) * 0.5556, 2)
        humidity = round((int(hex_data[index+12:index+16], 16) / 10), 2)
        co2 = int(hex_data[index+20:index+24], 16)
        index = index + 24

        # handle checksum after 20 records
        if n >= 19:  # end of 20 records
            index = index + 2
            n = 0
        else:
            n = n + 1

        record = [timestamp, temp, humidity, co2]
        # print(record)

        # check if new record or old one (to handle overwriting of first 20 records)
        if len(latest_record) > 0:
            if record[0] > latest_record[0]:  # new record - add to db for upload to cloud
                # print(str(record[0]) + " is greater than " + str(latest_record[0]))
                # print(record)
                # databaseWrite(prev_record)
                # results.append(record)
                latest_record = record
        else:
            # print(record)
            # databaseWrite(record)
            # results.append(record)
            latest_record = record

    # write latest record to DB

    if not latest_record:
      print("No data returned from rotronics device");
    else:
      print("storing to DB: ")
      print(latest_record)
      databaseWrite(latest_record)


def parse_go(go):
    return go.encode('hex')


def databaseWrite(record):
    db = sqlite3.connect(sql_db)
    cursor = db.cursor()
    cursor.execute('CREATE TABLE IF NOT EXISTS readings(timestamp INTEGER, temp INTEGER, humidity INTEGER, co2 INTEGER)')
    # check if database is full
    cursor.execute('SELECT count(*) FROM readings')
    result = cursor.fetchone()
    if result:
        # print("database size = "+ str(result[0]))
        if result[0] > 9000:
            # remove oldest reading
            cursor.execute('SELECT ROWID, * FROM readings ORDER BY ROWID ASC LIMIT 1');
            result = cursor.fetchone()
            cursor.execute('DELETE FROM readings WHERE ROWID='+ str(result[0]))
    cursor.execute('INSERT INTO readings VALUES (?, ?, ?, ?)', record)
    db.commit()
    cursor.close()
    db.close()


# **********************************
# main method
# **********************************
def main():
    # db = sqlite3.connect(sql_db)

    # stop logging
    # download data and store in local DB
    # then restart logging
    print("Stopping logging...")
    send_q()  # in order to quit logging before downloading data
    Q = parse_quit(get_response(0))

    if Q == 'Q':
        print("Stopped logging")
        print("\nDownloading data...")
        send_d()
        d = parse_data(get_response(2))
        # print(d)

        # store data in DB

        print("\nRestarting logging with clear memory")
        send_g()
        g = parse_go(get_response(0))
        print(g)
        if g == '53001e43024a':
            print("Logging restarted")
        else:
            print("Error restarting logging - logging not restarted")

    else:
        print("Error stopping logging - could not download data")


if __name__ == '__main__':
    # port = raw_input("\n\nPlease enter USB port name: ")
    port = "/dev/ttyUSB0"
    baud = 19200

    try:
        ser = serial.Serial(port, baud, bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE, parity=serial.PARITY_NONE, timeout=3)
        print("Opened port: " + ser.name)
        main()
    except serial.SerialException:
        print("Could not open port: " + port)
        exit(1)
