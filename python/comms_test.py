import serial
import time


# **********************************
# Send commands to rotronics device
# **********************************
def send_i():
    command = '6997'
    print("sending command: " + command)
    ser.write(bytearray.fromhex(command))


def send_s():
    command = '738d'
    print("sending command: " + command)
    ser.write(bytearray.fromhex(command))


def send_q():
    command = '51af'
    print("sending command: " + command)
    ser.write(bytearray.fromhex(command))


def send_d():
    command = '649c'
    print("sending command: " + command)
    ser.write(bytearray.fromhex(command))


def send_g():
    command = '53001E43024A'
    print("sending command: " + command)
    ser.write(bytearray.fromhex(command))


# **********************************
# Parse responses from rotronics device
# **********************************
def get_response(delay):
    res = ser.read()  # wait for response to arrive until timeout
    while True:
        time.sleep(delay)
        if ser.in_waiting > 0:
            res += ser.read(ser.in_waiting)
        else:
            return res


def parse_quit(q):
    return q.decode('latin-1')[:1]


def parse_info(info):
    return info.decode('latin-1')[1:-1]


def parse_settings(settings):
    settings_start = int(settings.encode('hex')[2:10], 16)
    settings_interval = int(settings.encode('hex')[10:14], 16)
    settings_records = int(settings.encode('hex')[14:18], 16)
    settings_units = chr(int(settings.encode('hex')[18:20], 16))
    return settings_start, settings_interval, settings_records, settings_units


def parse_data(data):
    hex_data = data.encode('hex')

    results = []
    index = 0  # handle start entry
    n = 0

    # data arrives in chunks of 20 with start byte and checksum byte
    # at beginning and end of each 20 records
    while index < len(hex_data):

        if n == 0:  # start of next 20 records
            index = index + 2

        timestamp = int(hex_data[index:index+8], 16) + 946684800  # converting 2000 time to UNIX time
        temp = round(((int(hex_data[index+8:index+12], 16) / 10) - 32) * 0.5556, 2)
        humidity = round((int(hex_data[index+12:index+16], 16) / 10), 2)
        co2 = int(hex_data[index+20:index+24], 16)
        index = index + 24

        # handle checksum after 20 records
        if n >= 19:  # end of 20 records
            index = index + 2
            n = 0
        else:
            n = n + 1

        record = [timestamp, temp, humidity, co2]

        # check if new record or old one (to handle overwriting of first 20 records)
        if len(results) > 0:
            if record[0] > results[len(results)-1][0]:
                # print(record)
                results.append(record)
            # else:
            #     print("Discarded record:")
            #     print(record)
        else:
            # print(record)
            results.append(record)

    return results


def parse_go(go):
    return go.encode('hex')


# **********************************
# main method
# **********************************
def main():
    while True:
        user_command = raw_input("\n\nPlease type command <i/s/q/d/g/quit>: ")

        # i = info
        # s = status
        # q = quit logging
        # d = get data
        # g = start logging
        # quit = quit script

        if user_command == 'i':
            send_i()
            deviceID = parse_info(get_response(0))
            print("response: ")
            print("Device ID = " + deviceID)

        elif user_command == 's':
            send_s()
            start, interval, records, units = parse_settings(get_response(0))
            print("response: ")
            print("start time (secs since 01/01/2000) = " + str(start))
            print("interval (secs) = " + str(interval))
            print("num records = " + str(records))
            print("units = " + str(units))

        elif user_command == 'q':
            print("Stopping logging...")
            send_q()
            result = parse_quit(get_response(0))
            if result == 'Q':
                print("logging stopped...")
            else:
                print("error stopping logging, still logging")

        elif user_command == 'd':
            print("Stopping logging...")
            send_q()  # in order to quit logging before downloading data
            Q = parse_quit(get_response(0))

            if Q == 'Q':
                print("Stopped logging")
                print("\nDownloading data...")
                send_d()
                d = parse_data(get_response(2))
                print(d)

                print("\nRestarting logging with clear memory")
                send_g()
                g = parse_go(get_response(0))
                if g == '53001e43024a':
                    print("Logging restarted")
                else:
                    print("Error restarting logging - logging not restarted")

            else:
                print("Error stopping logging - could not download data")

        elif user_command == 'g':
            print("Starting logging with clear memory")
            send_g()
            g = parse_go(get_response(1))
            print(g)
            print("response: OK")

        elif user_command == 'quit':
            print("bye!")
            exit(0)

        else:
            print("command not found, please try again")


if __name__ == '__main__':
    port = raw_input("\n\nPlease enter USB port name: ")
    baud = 19200

    try:
        ser = serial.Serial(port, baud, bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE, parity=serial.PARITY_NONE, timeout=3)
        print("Opened port: " + ser.name)
        main()
    except serial.SerialException:
        print("Could not open port: " + port)
        exit(1)



