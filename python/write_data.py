import sqlite3
from subprocess import call

# sql_db = '\Users\Sarah.Gallacher\data\rotronicsDB'
sql_db = '/home/pi/data/rotronicsDB'


def cloudUpload(record):
    #call Arduino script
    #print(record)
    args = map(str, record)
    call(["/home/pi/dev/bps-awayday/c/rotronics-send", args[0], args[1], args[2]])


def main():
    db = sqlite3.connect(sql_db)

    # check DB for entries
    c = db.cursor()
    try:
        c.execute('SELECT ROWID, * FROM readings ORDER BY ROWID DESC LIMIT 1')
        result = c.fetchone()
        # print(result)
    except:
        print("Table does not exist: readings")
        return None

    if result:
        # upload data via loraWAN
        # check if upload successful and delete from DB
        print("Uploading data:")
	print(result[1:])  # rowID is result[0]
        cloudUpload(result[1:])
    else:
        print("No data in table: readings")

    c.close()
    db.close()


if __name__ == '__main__':
    main()
